import React from "react";
// import { Link as RouterLink } from "react-router-dom";
import { Box, Flex, Image, Text } from "@chakra-ui/react";
import { consvertSecondToHrAndMinutes } from "../../../utils/dateAndTime";

export function AdsPlaying(props: any) {
  const { video } = props;
  // console.log("video : ", JSON.stringify(video));
  const playingTime = consvertSecondToHrAndMinutes(
    video.duration * video.totalSlotBooked
  ); // sending time in seconds only and get in hr, mn,sec

  return (
    <Box
      p="2"
      width="100%"
      bgColor="#F6F5F5"
      borderRadius="12px"
      boxShadow="2xl"
      key={video._id}
    >
      <Image width="100%" src={video.thumbnail} alt=""></Image>
      <Text
        color="#403F49"
        pt="2"
        fontSize="sm"
        fontWeight="semibold"
        align="left"
      >
        {video.campaignName}
      </Text>
      <Flex justifyContent="space-between" pt="2">
        <Text color="#403F49" fontSize="sm" align="left">
          Playtime
        </Text>
        <Text color="#403F49" fontSize="sm" align="left">
          {/* {`${video.hrsToComplete} hours`} */}
          {`${playingTime}`}
        </Text>
      </Flex>
      <Flex justifyContent="space-between" pt="2" align="center">
        <Text color="#403F49" fontSize="sm" align="left">
          Total no of slots
        </Text>
        <Text color="#403F49" fontSize="sm" align="left">
          {video.totalSlotBooked}
        </Text>
      </Flex>
    </Box>
  );
}
