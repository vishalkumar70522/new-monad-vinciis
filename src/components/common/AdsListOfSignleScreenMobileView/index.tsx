import {
  Card,
  CardBody,
  Stack,
  Text,
  HStack,
  Flex,
  Tooltip,
  IconButton,
} from "@chakra-ui/react";
import { BsPauseCircle } from "react-icons/bs";
import { RiDeleteBin6Line } from "react-icons/ri";
import { FaPlayCircle } from "react-icons/fa";
import { convertIntoDateAndTime } from "../../../utils/dateAndTime";
import { BsDot } from "react-icons/bs";
import { useState } from "react";

export function AdsListOfSignleScreenMobileView(props: any) {
  const { videos } = props;
  const [selectedIndex, setSelectedIndex] = useState<any>(-1);
  return (
    <Stack spacing={3}>
      {videos?.length > 0
        ? videos.map((video: any, index: any) => (
            <Card
              bg="#F9F9F9"
              onClick={() =>
                setSelectedIndex(index === selectedIndex ? -1 : index)
              }
            >
              <CardBody>
                <HStack justifyContent="space-between">
                  <Stack>
                    <Text
                      color="#403F49"
                      fontWeight="semibold"
                      fontSize="md"
                      align="left"
                    >
                      {video.campaignName}
                    </Text>
                    <Text color="#575757" fontSize="sm" align="left">
                      {convertIntoDateAndTime(video.startDate)}
                    </Text>
                    {selectedIndex === index ? (
                      <>
                        <Text
                          color="#403F49"
                          fontWeight="semibold"
                          fontSize="sm"
                          align="left"
                        >
                          {video.totalSlotBooked} slots
                        </Text>
                        <HStack justifyContent="space-between">
                          <Text
                            color="#403F49"
                            fontWeight="semibold"
                            fontSize="sm"
                            align="left"
                          >
                            ₹ {video.totalAmount}
                          </Text>
                        </HStack>
                        {props?.show ? (
                          <Flex gap={3}>
                            {!video.isPause ? (
                              <Tooltip
                                hasArrow
                                label="Pause campaign"
                                fontSize="md"
                                bg="gray.300"
                                color="black"
                              >
                                <IconButton
                                  bg="none"
                                  icon={
                                    <BsPauseCircle size="25px" color="black" />
                                  }
                                  aria-label="Home"
                                  onClick={() =>
                                    props?.handleChangeCampaignStatus(
                                      video._id,
                                      "Pause"
                                    )
                                  }
                                ></IconButton>
                              </Tooltip>
                            ) : (
                              <Tooltip
                                hasArrow
                                label="Resume campaign"
                                fontSize="md"
                                bg="gray.300"
                                color="black"
                              >
                                <IconButton
                                  bg="none"
                                  icon={
                                    <FaPlayCircle size="25px" color="black" />
                                  }
                                  aria-label="Home"
                                  onClick={() =>
                                    props?.handleChangeCampaignStatus(
                                      video._id,
                                      "Resume"
                                    )
                                  }
                                ></IconButton>
                              </Tooltip>
                            )}
                            <Tooltip
                              hasArrow
                              label="Delete campaign"
                              fontSize="md"
                              bg="gray.300"
                              color="black"
                            >
                              <IconButton
                                bg="nome"
                                icon={
                                  <RiDeleteBin6Line size="25px" color="red" />
                                }
                                aria-label="Home"
                                onClick={() =>
                                  props?.handleChangeCampaignStatus(
                                    video._id,
                                    "Deleted"
                                  )
                                }
                              ></IconButton>
                            </Tooltip>
                          </Flex>
                        ) : null}
                      </>
                    ) : null}
                  </Stack>
                  <Flex mt="0" pt="0">
                    <BsDot
                      size="25px"
                      color={video.status === "Deleted" ? "#E93A03" : "#00D615"}
                    />
                    <Text color="#403F45" fontSize="sm" pl="2">
                      {video.status}
                    </Text>
                  </Flex>
                </HStack>
              </CardBody>
            </Card>
          ))
        : null}
    </Stack>
  );
}
