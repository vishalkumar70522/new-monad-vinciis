import React, { useEffect, useState } from "react";
import {
  Box,
  Flex,
  Stack,
  Button,
  Text,
  Show,
  Hide,
  HStack,
  Select,
} from "@chakra-ui/react";
import { DashBoard } from "./DashBoard";
import { History } from "./History";
import { Actions } from "./Actions";
import { useNavigate } from "react-router-dom";
import { createScreen, getScreenLogs } from "../../Actions/screenActions";
import { useDispatch, useSelector } from "react-redux";
import { SCREEN_CREATE_RESET } from "../../Constants/screenConstants";
import HLoading from "../../components/atoms/HLoading";
import { userScreensList } from "../../Actions/userActions";
import MessageBox from "../../components/atoms/MessageBox";
import { getCampaignListByScreenId } from "../../Actions/campaignAction";
import { convertIntoDateAndTime } from "../../utils/dateAndTime";
import { Link as RouterLink } from "react-router-dom";

export function ScreenOwner() {
  const navigate = useNavigate();
  const [selectedScreen, setSelectedScreen] = useState<any>("");
  const [dashboard, setDashboard] = useState<any>(true);
  const [histoty, setHistory] = useState<any>(false);
  const [actions, setActions] = useState<any>(false);
  const [screen, setScreen] = useState<any>({});
  const [loading, setLoading] = useState<any>(false);
  const [selectedScreenIndex, setSelectedScreenIndex] = useState<any>(-1);

  const userSignin = useSelector((state: any) => state.userSignin);
  const { userInfo } = userSignin;
  //console.log("userInfo", JSON.stringify(userInfo));
  const userScreens = useSelector((state: any) => state.userScreens);
  const { loading: loadingScreens, error: errorScreens, screens } = userScreens;
  const activeCampaignListByScreenID = useSelector(
    (state: any) => state.activeCampaignListByScreenID
  );
  const {
    loading: loadingCampaign,
    error: errorCampaign,
    campaigns: videosList,
  } = activeCampaignListByScreenID;
  //console.log("screens 333 : ", JSON.stringify(screens));

  const screenCreate = useSelector((state: any) => state.screenCreate);
  const {
    loading: loadingCreate,
    error: errorCreate,
    success: successCreate,
    screen: createdScreen,
  } = screenCreate;

  const screenLogs = useSelector((state: any) => state.screenLogs);
  const {
    loading: loadingScreenLogs,
    error: errorScreenLogs,
    screenLog,
  } = screenLogs;

  const dispatch = useDispatch<any>();

  const handleDashboardClick = (e: any) => {
    setDashboard(true);
    setHistory(false);
    setActions(false);
  };
  const handleHistoryClick = (e: any) => {
    setDashboard(false);
    setHistory(true);
    setActions(false);
  };
  const handleActionClick = (e: any) => {
    setDashboard(false);
    setHistory(false);
    setActions(true);
  };
  const handleCreateScree = () => {
    dispatch(createScreen());
  };
  const handleSelectScreen = (screen: any, index: any) => {
    setSelectedScreenIndex(index);
    setScreen(screen);
    setSelectedScreen(screen._id);
    dispatch(getCampaignListByScreenId(screen._id));
    dispatch(getScreenLogs(screen._id));
  };
  const handleSelectScreenForMobile = (index: any) => {
    setSelectedScreenIndex(index);
    setScreen(screens[index]);
    setSelectedScreen(screens[index]?._id);
    dispatch(getCampaignListByScreenId(screens[index]?._id));
    dispatch(getScreenLogs(screens[index]?._id));
  };

  useEffect(() => {
    if (!userInfo) {
      navigate("/signin");
    }
    if (successCreate) {
      dispatch({ type: SCREEN_CREATE_RESET });
      navigate(`/edit-screen/${createdScreen._id}`);
    }
    //console.log("screens : ", screens);
    if (screens?.length > 0) {
      // console.log("came inside when we get screens data");
      handleSelectScreen(screens[0], 0);
    }
    dispatch(userScreensList(userInfo));
    setLoading(true);
  }, [successCreate, dispatch, navigate, userInfo, loading]);

  return (
    <Box pt={{ base: "3", lg: "5" }}>
      <Box px={{ base: "3", lg: "20" }}>
        {loadingScreens ||
        loadingCreate ||
        loadingCampaign ||
        loadingScreenLogs ? (
          <HLoading
            loading={
              loadingScreens ||
              loadingCreate ||
              loadingCampaign ||
              loadingScreenLogs
            }
          />
        ) : errorScreens || errorCreate || errorCampaign || errorScreenLogs ? (
          <MessageBox variant="danger">
            {errorScreens || errorCreate || errorCampaign || errorScreenLogs}
          </MessageBox>
        ) : (
          <>
            <Show below="md">
              <Box>
                <HStack align="left" justifyContent="space-around">
                  <Select
                    color="#000000"
                    size="lg"
                    width="70%"
                    py="3"
                    boxShadow="2xl"
                    onChange={(e) =>
                      handleSelectScreenForMobile(e.target.value)
                    }
                  >
                    <option value={-1}>----Select Screeen -----</option>
                    {screens?.map((eachScreen: any, index: any) => (
                      <option value={index} key={eachScreen._id}>
                        {eachScreen?.name}
                      </option>
                    ))}
                  </Select>
                  <Button
                    color="#000000"
                    _hover={{ bg: "#82CD47", color: "#FFFFFF" }}
                    fontSize={{ base: "xs", lg: "sm" }}
                    boxShadow="xl"
                    alignContent="center"
                    variant="outline"
                    px="5"
                    onClick={handleCreateScree}
                  >
                    + New
                  </Button>
                </HStack>
                {selectedScreen ? (
                  <HStack
                    p="2"
                    mt="5"
                    borderRadius="8px"
                    boxShadow="2xl"
                    justifyContent="space-around"
                    align="left"
                  >
                    <Stack as={RouterLink} to={`/screen/${screen?._id}`}>
                      <Text
                        fontSize="lg"
                        fontWeight="semibold"
                        color="#403F49"
                        align="left"
                      >
                        {screen?.name}
                      </Text>
                      <Text
                        fontSize="sm"
                        fontWeight="semibold"
                        color="#403F49"
                        align="left"
                      >
                        {`${screen?.size?.length} X ${screen?.size?.width}`}
                      </Text>
                      <Text
                        fontSize="14px"
                        fontWeight="semibold"
                        color="#333333"
                        align="left"
                      >
                        {`${screen?.slotsTimePeriod}sec/slot`}
                      </Text>
                      <Text
                        fontSize="14px"
                        fontWeight="semibold"
                        color="#403F49"
                        align="left"
                      >
                        {
                          convertIntoDateAndTime(screen.startTime)?.split(
                            ","
                          )[2]
                        }{" "}
                        to{" "}
                        {convertIntoDateAndTime(screen.endTime)?.split(",")[2]}
                      </Text>
                    </Stack>
                    <Stack>
                      <Button
                        fontSize="sm"
                        variant="link"
                        py="3"
                        color="#333333"
                        onClick={() => {
                          if (selectedScreen) {
                            navigate(`/edit-screen/${selectedScreen}`);
                          } else {
                            alert("First select screen for edit..");
                          }
                        }}
                      >
                        Edit screen
                      </Button>
                    </Stack>
                  </HStack>
                ) : null}

                <HStack
                  borderRadius="48px"
                  boxShadow="2xl"
                  align="left"
                  mt="5"
                  bgColor="#FBFBFB"
                >
                  <Button
                    variant="outline"
                    fontSize="sm"
                    fontWeight={dashboard ? "semibold" : "normal"}
                    color={dashboard ? "#000000" : "#333333"}
                    borderColor={dashboard ? "#0EBCF5" : ""}
                    border={dashboard ? "1px" : ""}
                    borderRadius="48px"
                    bgColor="#FBFBFB"
                    px="5"
                    py="3"
                    onClick={handleDashboardClick}
                  >
                    DashBoard
                  </Button>
                  <Button
                    variant="outline"
                    fontSize="sm"
                    fontWeight={histoty ? "semibold" : "normal"}
                    color={histoty ? "#000000" : "#333333"}
                    borderColor={histoty ? "#0EBCF5" : ""}
                    border={histoty ? "1px" : ""}
                    borderRadius="48px"
                    bgColor="#FBFBFB"
                    px="5"
                    py="3"
                    onClick={handleHistoryClick}
                  >
                    History
                  </Button>
                  <Button
                    variant="outline"
                    fontSize="sm"
                    fontWeight={actions ? "semibold" : "normal"}
                    color={actions ? "#000000" : "#333333"}
                    borderColor={actions ? "#0EBCF5" : ""}
                    border={actions ? "1px" : ""}
                    borderRadius="48px"
                    bgColor="#FBFBFB"
                    px="9"
                    py="3"
                    onClick={handleActionClick}
                  >
                    Actions
                  </Button>
                </HStack>

                <Box>
                  {dashboard && selectedScreen ? (
                    <DashBoard screen={screen} videosList={videosList} />
                  ) : histoty && selectedScreen ? (
                    <History screen={screen} screenLog={screenLog} />
                  ) : actions && selectedScreen ? (
                    <Actions
                      screen={screen}
                      videosList={videosList}
                      handleSelectScreen={handleSelectScreen}
                      selectedScreenIndex={selectedScreenIndex}
                    />
                  ) : null}
                </Box>
              </Box>
            </Show>
            <Hide below="md">
              <Flex>
                <Stack
                  p="5"
                  bgColor="#FEFEFE"
                  width={{ base: "70%", lg: "20%" }}
                  boxShadow="2xl"
                >
                  <Text
                    fontSize={{ base: "sm", lg: "lg" }}
                    fontWeight="semibold"
                    color="#000000"
                    align="left"
                    pt="20"
                  >
                    All Screen
                  </Text>
                  <Button
                    color="#000000"
                    _hover={{ bg: "#82CD47", color: "#FFFFFF" }}
                    fontSize={{ base: "xs", lg: "sm" }}
                    boxShadow="2xl"
                    alignContent="center"
                    variant="outline"
                    p="3"
                    onClick={handleCreateScree}
                  >
                    + New
                  </Button>
                  <Stack pt="10">
                    {screens?.map((eachScreen: any, index: any) => (
                      <Text
                        fontSize="sm"
                        fontWeight="regular"
                        key={index + 1}
                        color={
                          index === selectedScreenIndex ? "#753972" : "#403F49"
                        }
                        align="left"
                        p="3"
                        borderRadius="8px"
                        bg={index === selectedScreenIndex ? "#0EBCF5" : ""}
                        _hover={{
                          bg: "rgba(14, 188, 245, 0.3)",
                          color: "#674780",
                        }}
                        onClick={() => {
                          handleSelectScreen(eachScreen, index);
                        }}
                      >
                        {eachScreen.name}
                      </Text>
                    ))}
                  </Stack>
                </Stack>
                <Stack p="5" width="100%">
                  <Stack
                    direction="row"
                    pt=""
                    align="center"
                    justifyContent="space-between"
                  >
                    <Stack></Stack>
                    <Stack
                      direction="row"
                      borderRadius="48px"
                      boxShadow="2xl"
                      align="center"
                      justifyContent="center"
                      bgColor="#FBFBFB"
                    >
                      <Button
                        variant="outline"
                        fontSize="sm"
                        fontWeight={dashboard ? "semibold" : "normal"}
                        color={dashboard ? "#000000" : "#333333"}
                        borderColor={dashboard ? "#0EBCF5" : ""}
                        border={dashboard ? "1px" : ""}
                        borderRadius="48px"
                        bgColor="#FBFBFB"
                        p="3"
                        onClick={handleDashboardClick}
                      >
                        DashBoard
                      </Button>
                      <Button
                        variant="outline"
                        fontSize="sm"
                        fontWeight={histoty ? "semibold" : "normal"}
                        color={histoty ? "#000000" : "#333333"}
                        borderColor={histoty ? "#0EBCF5" : ""}
                        border={histoty ? "1px" : ""}
                        borderRadius="48px"
                        bgColor="#FBFBFB"
                        p="3"
                        onClick={handleHistoryClick}
                      >
                        History
                      </Button>
                      <Button
                        variant="outline"
                        fontSize="sm"
                        fontWeight={actions ? "semibold" : "normal"}
                        color={actions ? "#000000" : "#333333"}
                        borderColor={actions ? "#0EBCF5" : ""}
                        border={actions ? "1px" : ""}
                        borderRadius="48px"
                        bgColor="#FBFBFB"
                        p="3"
                        onClick={handleActionClick}
                      >
                        Actions
                      </Button>
                    </Stack>
                    <Stack
                      direction="row"
                      align="center"
                      alignItems="flex-end"
                      justifyContent="flex-end"
                      pr="10"
                    >
                      <Button
                        fontSize="sm"
                        variant="outline"
                        py="3"
                        fontWeight="semibold"
                        color="#333333"
                        _hover={{ bg: "red", color: "#FFFFFF" }}
                        onClick={() => {
                          if (selectedScreen) {
                            navigate(`/edit-screen/${selectedScreen}`);
                          } else {
                            alert("First select screen for edit..");
                          }
                        }}
                      >
                        Edit screen
                      </Button>
                    </Stack>
                  </Stack>
                  <Box>
                    {dashboard && selectedScreen ? (
                      <DashBoard screen={screen} videosList={videosList} />
                    ) : histoty && selectedScreen ? (
                      <History screen={screen} screenLog={screenLog} />
                    ) : actions && selectedScreen ? (
                      <Actions
                        screen={screen}
                        videosList={videosList}
                        handleSelectScreen={handleSelectScreen}
                        selectedScreenIndex={selectedScreenIndex}
                      />
                    ) : null}
                  </Box>
                </Stack>
              </Flex>
            </Hide>
          </>
        )}
      </Box>
    </Box>
  );
}
