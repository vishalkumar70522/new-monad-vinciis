import { Button, Center, Flex, Image, Text } from "@chakra-ui/react";
import { useNavigate } from "react-router-dom";
export function OneCoupon(props: any) {
  const { singlData } = props;
  const navigate = useNavigate();
  return (
    <Center
      border="1px"
      borderColor="#D9D9D9"
      p="5"
      _hover={{ boxShadow: "2xl" }}
      borderRadius="8px"
      width={{ base: "185px", lg: "307px" }}
      height={{ base: "280px", lg: "357px" }}
      flexDirection="column"
    >
      <Image src={singlData?.image} alt=" " height="96px" />
      <Flex direction="column">
        <Text
          pt="5"
          color="#303030"
          fontSize={{ base: "12px", lg: "lg" }}
          fontWeight="638"
          m="0"
        >
          {singlData?.brand}
        </Text>
      </Flex>
      <Text
        py="3"
        color="#585858"
        fontSize={{ base: "12px", lg: "md" }}
        fontWeight="400"
      >
        {singlData?.offer}
      </Text>

      <Button
        color="#FFFFFF"
        bgColor="#D7380E"
        py={{ base: "2", lg: "3" }}
        fontSize={{ base: "", lg: "md" }}
        fontWeight="638"
        onClick={() =>
          navigate("/couponDetils", {
            state: {
              couponData: singlData,
            },
          })
        }
      >
        Get Deal
      </Button>
    </Center>
  );
}
