import {
  Box,
  Center,
  Text,
  Flex,
  Image,
  Stack,
  Button,
  Link,
  Hide,
  Show,
  SimpleGrid,
  Tooltip,
} from "@chakra-ui/react";
import { useState } from "react";
import { AiOutlineClockCircle } from "react-icons/ai";
import { BsDot, BsShare } from "react-icons/bs";
import { GiCircleCage } from "react-icons/gi";
import { useLocation, useNavigate } from "react-router-dom";
import { useSelector } from "react-redux";
import { ImageSliderPopup } from "./ImageSliderPopup";

const aboutOffer = `Lorem ipsum dolor sit amet consectetur. Sed ultricies integer purus quam id diam. 
Est hendrerit in hac eu leo. At commodo senectus tincidunt tempus massa diam. 
Felis sit non pulvinar et ipsum at. Nisl donec scelerisque id non blandit dui.
Felis sit non pulvinar et ipsum at. Nisl donec scelerisque id non blandit dui.`;

export function CouponDetailsDesktopView(props: any) {
  const [show, setShow] = useState<any>(false);
  const navigate = useNavigate();
  const [imageShow, setImageShow] = useState<any>(false);
  const currentPath = window.location.pathname;
  const location = useLocation();
  const couponData = props?.couponData || location?.state?.data;
  const [isCopied, setIsCopied] = useState<any>(false);

  const onCopy = (value: any) => {
    setIsCopied(true);
    navigator.clipboard.writeText(value);
  };

  const userSignin = useSelector((state: any) => state.userSignin);
  const {
    loading: loadingUserInfo,
    error: errorUserInfo,
    userInfo,
  } = userSignin;
  const handelGetDel = () => {
    if (!userInfo) {
      navigate("/signin", {
        state: {
          path: currentPath,
          data: couponData,
        },
      });
    } else {
      setShow(true);
    }
  };
  return (
    <Box px={{ base: "2", lg: "20" }} pt={{ base: "3", lg: "5" }}>
      <ImageSliderPopup
        imageShow={imageShow}
        setImageShow={(value: any) => setImageShow(value)}
        images={couponData?.images}
      />
      <Stack align="center">
        <Box
          border="1px"
          p="10"
          borderColor="#E6E6E6"
          width="331px"
          height="201px"
          alignItems="center"
          justifyContent="center"
          borderRadius="4px"
        >
          <Center>
            <Image
              src={couponData.image}
              alt="berger"
              height={{ base: "100px", lg: "123px" }}
            ></Image>
          </Center>
        </Box>
        <a href={couponData?.brandLink}>
          <Text color="#000000" m="0" fontSize="2xl" fontWeight="530">
            {couponData?.brand}
          </Text>
        </a>
        <Text color="#585858" m="0" fontSize="lg" fontWeight="400">
          {couponData?.offer}
        </Text>
        {show ? (
          <Box pt="5">
            <Flex>
              <Text
                color="#000000"
                m="0"
                fontSize="md"
                px="5"
                py="3"
                fontWeight="400"
                align="center"
                border="1px"
                borderColor="#D9D9D9"
                borderLeftRadius="48px"
              >
                {couponData?.couponCode}
              </Text>
              <Button
                borderEndRadius="48px"
                color="#FFFFFF"
                bgColor="#D7380E"
                py="1"
                fontSize={{ base: "", lg: "md" }}
                fontWeight="638"
                onClick={() => onCopy(couponData?.couponCode)}
              >
                {isCopied ? "Copied!" : "Copy"}
              </Button>
            </Flex>
            <Text color="#484848" m="0" fontSize="sm" fontWeight="511" pt="5">
              Visit{" "}
              <Link color="teal.500" href={couponData?.brandLink}>
                {couponData?.brand}
              </Link>{" "}
              Brand and paste your code
            </Text>
            <Flex
              gap={{ base: "5", lg: "5" }}
              alignContent="center"
              justifyContent="center"
              align="center"
            >
              <AiOutlineClockCircle color="#484848" />
              <Text
                color="#484848"
                m="0"
                fontSize={{ base: "12px", lg: "sm" }}
                fontWeight="511"
                pt="2"
              >
                Expires in 7 days{" "}
              </Text>
            </Flex>
          </Box>
        ) : (
          <Box pt="5">
            <Button
              color="#FFFFFF"
              bgColor="#D7380E"
              py="3"
              px="10"
              onClick={() => handelGetDel()}
            >
              GET DEAL
            </Button>
          </Box>
        )}
      </Stack>

      <Box
        px={{ base: "2", lg: "10" }}
        py={{ base: "3", lg: "10" }}
        mt="5"
        border="1px"
        borderColor="#D9D9D9"
        borderRadius="4px"
      >
        <Text
          color="#969696"
          m="0"
          fontSize="2xl"
          fontWeight="650"
          align="left"
        >
          About brand
        </Text>
        <Flex justifyContent="space-between" alignContent="center" mt="5">
          <Flex gap={{ base: "5", lg: "7" }}>
            <Image
              src={couponData?.image}
              alt="berger"
              height="82px"
              width="82px"
            ></Image>
            <Box>
              <Text
                color="#000000"
                m="0"
                fontSize={{ base: "md", lg: "2xl" }}
                fontWeight="530"
                align="left"
              >
                {couponData?.brand}
              </Text>
              <Hide below="md">
                <Flex pt={{ base: "2", lg: "5" }} gap={{ base: "2", lg: "5" }}>
                  <Button
                    color="#FFFFFF"
                    bgColor="#D7380E"
                    py="3"
                    fontSize={{ base: "", lg: "md" }}
                    fontWeight="638"
                    px="10"
                    leftIcon={<GiCircleCage color="#FFFFFF" size="16px" />}
                    onClick={() => window.open(couponData.brandLink, "_blank")}
                  >
                    Visit online store
                  </Button>
                </Flex>
              </Hide>
            </Box>
          </Flex>
          <Flex gap="2">
            <Text
              color="#313131"
              m="0"
              fontSize={{ base: "12px", lg: "sm" }}
              fontWeight="530"
              align="left"
            >
              Share
            </Text>
            <BsShare size="16px" color="#313131" />
          </Flex>
        </Flex>
        <Show below="md">
          <Flex pt={{ base: "2", lg: "5" }} gap={{ base: "2", lg: "5" }}>
            <Button
              color="#FFFFFF"
              bgColor="#D7380E"
              py="3"
              fontSize={{ base: "", lg: "md" }}
              fontWeight="638"
              px="10"
              leftIcon={<GiCircleCage color="#FFFFFF" size="16px" />}
              onClick={() => window.open(couponData.brandLink, "_blank")}
            >
              Visit online store
            </Button>
          </Flex>
        </Show>
        <Flex gap="1" direction="column">
          <Text
            color="#676767"
            m="0"
            fontSize={{ base: "12px", lg: "md" }}
            fontWeight="400"
            align="left"
            pt="5"
            noOfLines={[3, 2]}
            // isTruncated={false}
          >
            The Quirkiest Sneaker Brand Revolutionizing Footwear Trends!
            Funkfeets is a fresh burst of creativity into the world of sneakers,
            It dares to challenge conventional shoe designs and embraces the
            extraordinary. At Funkfeets, we believe that shoes should never be
            boring or predictable. We've discarded the notion of traditional,
            symmetrical designs and replaced them with vibrant, asymmetrical
            patterns that captivate the eye and spark conversations. Our
            sneakers are more than just footwear; they're an expression of your
            unique style and personality.
          </Text>

          {/* <Text
            color="#D7380E"
            m="0"
            fontSize={{ base: "12px", lg: "md" }}
            fontWeight="400"
            align="left"
            pt="5"
          >
            {" more"}
          </Text> */}
        </Flex>
        <SimpleGrid columns={[2, 2, 6]} spacing={{ base: "2", lg: "5" }} pt="5">
          {couponData?.images?.map((image: any, index: any) => (
            <Tooltip
              hasArrow
              label="See in full size"
              fontSize="md"
              bg="gray.300"
              color="black"
              key={index}
            >
              <Image
                src={image}
                borderRadius="8px"
                alt="berger"
                height="135px"
                width="172px"
                onClick={() => {
                  setImageShow(true);
                }}
              />
            </Tooltip>
          ))}
        </SimpleGrid>
      </Box>
      {show && (
        <Box
          px={{ base: "2", lg: "10" }}
          py={{ base: "3", lg: "10" }}
          mt="5"
          border="1px"
          borderColor="#D9D9D9"
          borderRadius="4px"
        >
          <Text
            color="#000000"
            m="0"
            fontSize={{ base: "sm", lg: "lg" }}
            fontWeight="565"
            align="left"
            p="5"
          >
            About this offer
          </Text>
          {aboutOffer?.split("\n").map((text: any) => (
            <Flex gap={2}>
              <BsDot color="#595959" />
              <Text
                color="#595959"
                m="0"
                fontSize={{ base: "12px", lg: "15px" }}
                fontWeight="400"
                align="left"
              >
                {text}
              </Text>
            </Flex>
          ))}
          <Text
            color="#000000"
            m="0"
            fontSize={{ base: "sm", lg: "lg" }}
            fontWeight="556"
            align="left"
            p="5"
          >
            How to get this offer
          </Text>
          {aboutOffer?.split("\n").map((text: any) => (
            <Flex gap={2}>
              <BsDot color="#595959" />
              <Text
                color="#595959"
                m="0"
                fontSize={{ base: "12px", lg: "15px" }}
                fontWeight="400"
                align="left"
              >
                {text}
              </Text>
            </Flex>
          ))}
          <Text
            color="#000000"
            m="0"
            fontSize={{ base: "sm", lg: "lg" }}
            fontWeight="556"
            align="left"
            p="5"
          >
            Terms and conditions
          </Text>
          {aboutOffer?.split("\n").map((text: any) => (
            <Flex gap={2}>
              <BsDot color="#595959" />
              <Text
                color="#595959"
                m="0"
                fontSize={{ base: "12px", lg: "15px" }}
                fontWeight="400"
                align="left"
              >
                {text}
              </Text>
            </Flex>
          ))}
        </Box>
      )}
    </Box>
  );
}
