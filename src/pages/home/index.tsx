import { Box } from "@chakra-ui/react";
import { useSelector } from "react-redux";
import { HomePage } from "./HomePage";
import { CouponHomePage } from "../CouponHomePage";

export function Home(props: any) {
  const userSignin = useSelector((state: any) => state.userSignin);
  const {
    loading: loadingUserInfo,
    error: errorUserInfo,
    userInfo,
  } = userSignin;

  return <Box>{userInfo?.isMaster ? <HomePage /> : <CouponHomePage />}</Box>;
}
