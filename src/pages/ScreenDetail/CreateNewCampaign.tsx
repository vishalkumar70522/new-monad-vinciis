import React, { useEffect, useState } from "react";
import { Modal } from "react-bootstrap";
import {
  Stack,
  IconButton,
  Text,
  FormControl,
  FormLabel,
  InputGroup,
  Input,
  Box,
  Button,
  SimpleGrid,
} from "@chakra-ui/react";
import { AiOutlineCloseCircle } from "react-icons/ai";
import MessageBox from "../../components/atoms/MessageBox";

export function CreateNewCampaign(props: any) {
  const [error, setError] = useState<any>("");

  const checkCampaign = () => {
    if (props.campaignName) {
      props.onHide();
      return true;
    } else {
      setError("Please Enter Campaign name!");
      setTimeout(() => {
        setError("");
        console.log("error = ", error);
      }, 2000);
      return false;
    }
  };

  const handleCreateCampaignThroughVideo = () => {
    if (checkCampaign()) {
      props.openUploadCampaignModal();
    }
  };
  const handleCreateCampaignThroughImages = () => {
    if (checkCampaign()) {
      props.openUploadCampaignThrougnImages();
    }
  };

  useEffect(() => {}, [error]);

  return (
    <Modal
      {...props}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      backdrop="static"
      keyboard={false}
      dialogClassName="modal-90w"
    >
      <Modal.Body>
        <Box bgColor="#FFFFFF">
          <Stack align="end" justifyContent="flex-end">
            <IconButton
              bg="none"
              icon={
                <AiOutlineCloseCircle
                  size="40px"
                  fontWeight="10"
                  color="black"
                  onClick={() => {
                    props.onHide();
                    props.setCampaignName("");
                  }}
                />
              }
              aria-label="Close"
            />
          </Stack>
          {error && <MessageBox variant="danger">{error}</MessageBox>}
          <Stack px={{ base: "3", lg: "20" }}>
            <Stack align="center">
              <Text fontSize="xl" fontWeight="semibold" color="#000000">
                Add campaign
              </Text>
            </Stack>
            <FormControl pt="5">
              <FormLabel htmlFor="share">Enter campaign name</FormLabel>
              <InputGroup>
                <Input
                  name="share"
                  id="share"
                  size="lg"
                  py="2"
                  value={props.campaignName}
                  onChange={(e: any) => props.setCampaignName(e.target.value)}
                  // onKeyPress={(e) => handleAddCampaignName(e, false)}
                />
              </InputGroup>
            </FormControl>
            <SimpleGrid
              columns={[1, 2]}
              spacing={5}
              pb={{ base: 5, lg: 10 }}
              pt={{ base: 5, lg: 10 }}
            >
              <Button p="3" onClick={handleCreateCampaignThroughImages}>
                Create campaign through Images
              </Button>
              <Button p="3" onClick={handleCreateCampaignThroughVideo}>
                Create campaign through Video
              </Button>
            </SimpleGrid>
          </Stack>
        </Box>
      </Modal.Body>
    </Modal>
  );
}
