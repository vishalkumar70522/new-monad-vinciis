import React from "react";
import {
  Box,
  Text,
  Stack,
  IconButton,
  Table,
  TableContainer,
  Tbody,
  Td,
  Th,
  Thead,
  Tr,
} from "@chakra-ui/react";
import { AiOutlineCloseCircle } from "react-icons/ai";
import { Modal } from "react-bootstrap";
import { convertIntoDateAndTime } from "../../utils/dateAndTime";

export function CampaignLogPopup(props: any) {
  const { campaignLogs } = props;
  const getDeviceId = (deviceInfo: string) => {
    const info = deviceInfo ? JSON.parse(deviceInfo) : null;
    return info ? info.deviceId : null;
  };
  const getDeviceIp = (deviceInfo: string) => {
    const info = deviceInfo ? JSON.parse(deviceInfo) : null;
    return info ? info.deviceIp : null;
  };
  const getDeviceDisplay = (deviceInfo: string) => {
    const info = deviceInfo ? JSON.parse(deviceInfo) : null;
    return info ? info.deviceDisplay : null;
  };
  return (
    <Modal {...props} size="xl" aria-labelledby="contained-modal-title-vcenter">
      <Modal.Body>
        <Box bgColor="#FFFFFF">
          {" "}
          <Stack align="end" justifyContent="flex-end">
            <IconButton
              bg="none"
              icon={
                <AiOutlineCloseCircle
                  size="40px"
                  fontWeight="10"
                  color="#00000090"
                  onClick={props.onHide}
                />
              }
              aria-label="Close"
            />
          </Stack>
          <Box color="black.500" p="10" boxShadow="2xl">
            {campaignLogs ? (
              <TableContainer borderRadius="5px" bgColor="#FFFFFF">
                <Table variant="simple" size="lg">
                  <Thead>
                    <Tr>
                      <Th>Device ID</Th>
                      <Th>Device </Th>
                      <Th>Playing Time</Th>
                      <Th>Address</Th>
                    </Tr>
                  </Thead>
                  <Tbody>
                    {campaignLogs
                      ?.reverse()
                      ?.map((SingleLog: any, index: any) => (
                        <Tr
                          key={index + 1}
                          onClick={() =>
                            props.handleShowCampaignLogs(SingleLog.cid)
                          }
                          _hover={{ bg: "rgba(14, 188, 245, 0.3)" }}
                        >
                          <Td>
                            <Text color=" #403F49 " fontSize="sm">
                              {getDeviceId(SingleLog.deviceInfo)}
                            </Text>
                          </Td>
                          <Td>
                            <Text color=" #403F49 " fontSize="sm">
                              {getDeviceDisplay(SingleLog.deviceInfo)}
                            </Text>
                          </Td>
                          <Td color="#575757" fontSize="sm">
                            {convertIntoDateAndTime(SingleLog.playTime)}
                          </Td>
                          <Td
                            isNumeric
                            fontSize="sm"
                            color="#403F49"
                            fontWeight="semibold"
                          >
                            {getDeviceIp(SingleLog.deviceInfo)}
                          </Td>
                        </Tr>
                      ))}
                  </Tbody>
                </Table>
              </TableContainer>
            ) : (
              <Text
                color="#D7380E"
                fontSize="lg"
                align="left"
                fontWeight="semibold"
                pt="5"
              >
                No Campaign Logs
              </Text>
            )}
          </Box>
        </Box>
      </Modal.Body>
    </Modal>
  );
}
