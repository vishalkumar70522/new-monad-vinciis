import { Navigate, Route, Routes } from "react-router-dom";
import { FooterPage, NavBar } from "../components/common";
import {
  AllAds,
  AllScreens,
  CampaignDetails,
  CampaignListOfUser,
  CartAndSummary,
  CreateResetPassword,
  EditScreen,
  GenerateWallet,
  KeyConfirm,
  KeyManagement,
  KeyPhraseSave,
  KeyRecovery,
  Login,
  Logout,
  Page404,
  PaymentReceipt,
  PinCreate,
  PinSuccess,
  RequestMoney,
  // ScreenDetail,
  ScreenDetailPage,
  ScreenOwner,
  SendMoney,
  Signin,
  Signup,
  ViewSecrateKey,
  WalletPage,
  Welcome,
  UserProfile,
  EditUserProfile,
  EmailVerificationForForgetPassword,
  Home,
  CouponDetails,
} from "../pages";
import { CouponHomePage } from "../pages/CouponHomePage";

export const PublicRoutes = () => {
  return (
    <>
      <NavBar />
      <Routes>
        <Route path="/create-wallet" element={<GenerateWallet />} />
        <Route path="/" element={<Home />} />
        <Route path="/welcome" element={<Welcome />} />
        <Route path="/key-management" element={<KeyManagement />} />
        <Route path="/key-phrase-save" element={<KeyPhraseSave />} />
        <Route path="/key-confirm" element={<KeyConfirm />} />
        <Route path="/key-recovery" element={<KeyRecovery />} />
        <Route path="/pin-create" element={<PinCreate />} />
        <Route path="/pin-success" element={<PinSuccess />} />
        <Route path="/view-secrate-key" element={<ViewSecrateKey />} />
        {/* auth */}
        <Route path="/login" element={<Login />} />
        <Route path="/logout" element={<Logout />} />
        <Route path="/signup" element={<Signup />} />
        <Route path="/signin" element={<Signin />} />
        <Route
          path="/create-reset-password/:email/:name"
          element={<CreateResetPassword />}
        />
        <Route
          path="/forgetPassword"
          element={<EmailVerificationForForgetPassword />}
        />

        <Route path="/home" element={<Navigate to="/" />} />
        <Route path="/allads" element={<AllAds />} />
        <Route path="/all-screens" element={<AllScreens />} />
        <Route path="/screen/:id" element={<ScreenDetailPage />} />
        <Route path="/screen-owner" element={<ScreenOwner />} />
        <Route path="/edit-screen/:id" element={<EditScreen />} />
        <Route
          path="/carts/:mediaId/:screenId/:name/:url"
          element={<CartAndSummary />}
        />
        <Route path="/myCampaignList" element={<CampaignListOfUser />} />
        <Route path="/campaignDetails/:id" element={<CampaignDetails />} />
        <Route path="/walletpage" element={<WalletPage />} />
        <Route path="/paymentreceipt/:id" element={<PaymentReceipt />} />
        <Route path="/send-money" element={<SendMoney />} />
        <Route path="/request-money" element={<RequestMoney />} />
        <Route path="/userprofile" element={<UserProfile />} />
        <Route path="/editProfile" element={<EditUserProfile />} />
        <Route path="/couponHomepage" element={<CouponHomePage />} />

        <Route path="/couponDetils" element={<CouponDetails />} />
        <Route path="/couponList" element={<CouponHomePage />} />

        <Route path="*" element={<Page404 />} />
      </Routes>
      <FooterPage />
    </>
  );
};
