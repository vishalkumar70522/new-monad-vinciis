import {
  Box,
  Flex,
  Stack,
  Text,
  FormControl,
  Menu,
  MenuButton,
  MenuItem,
  MenuList,
  Button,
  Hide,
  HStack,
  Show,
} from "@chakra-ui/react";
import { AdsListOfSinglScreen } from "../../components/common";
import React, { useEffect, useState } from "react";
import DateFnsUtils from "@date-io/date-fns"; // choose your lib
import { DateTimePicker, MuiPickersUtilsProvider } from "@material-ui/pickers";
import {
  IconButton as MiuiIconButton,
  InputAdornment,
} from "@material-ui/core";
import { AiOutlineDown } from "react-icons/ai";
import { useDispatch, useSelector } from "react-redux";
import {
  filteredCampaignListDateWise,
  getAllCampaignListByScreenId,
} from "../../Actions/campaignAction";
import HLoading from "../../components/atoms/HLoading";
import MessageBox from "../../components/atoms/MessageBox";
import { CampaignLogPopup } from "./CampaignLogPopup";
import { AdsListOfSignleScreenMobileView } from "../../components/common/AdsListOfSignleScreenMobileView";
import { FILTERED_CAMPAIGN_LIST_RESET } from "../../Constants/campaignConstants";
export function History(props: any) {
  const { screen, screenLog } = props;
  const dispatch = useDispatch<any>();
  const [startDateHere, setStartDateHere] = useState<any>(new Date());
  const [endDateHere, setEndDateHere] = useState<any>(new Date());
  const [showCampaignLogs, setShowCampaignLogs] = useState<any>(false);
  const [campaignLogs, setCampaignLogs] = useState<any>([]);
  const [filteredCampaignsList, setFilteredCampaignList] = useState<any>([]);
  const [myFilterError, setMyFilterError] = useState<any>("");
  const [filteredCampaignListByStatus, setFilteredCampaignListByStatus] =
    useState([]);

  const handleShowCampaignLogs = (cid: any) => {
    const logs = screenLog
      .filter((log: any) => log.playVideo.split(".")[0] === cid)
      .reverse();
    setCampaignLogs(logs);
    setShowCampaignLogs(true);
  };
  const filterCampaign = () => {
    // console.log("filterCampaign Called! ");
    setFilteredCampaignListByStatus([]);
    dispatch(
      filteredCampaignListDateWise(startDateHere, endDateHere, screen._id)
    );
  };
  const campaignListDateWise = useSelector(
    (state: any) => state.campaignListDateWise
  );
  const {
    loading: loadingFilteredCampaign,
    error: errorFilteredCampaign,
    campaigns: filteredCampaigns,
  } = campaignListDateWise;

  const filterCampaigndateBySatus = (value: any) => {
    if (value !== "All") {
      if (filteredCampaignsList?.length > 0) {
        const data = filteredCampaignsList?.filter(
          (campaign: any) => campaign.status === value
        );
        if (data?.length > 0) setFilteredCampaignListByStatus(data);
        else {
          setMyFilterError("No data found!");
          setFilteredCampaignListByStatus([]);
        }
      } else if (videosList?.length > 0) {
        const data = videosList?.filter(
          (campaign: any) => campaign.status === value
        );
        if (data?.length > 0) setFilteredCampaignListByStatus(data);
        else {
          setMyFilterError("No data found!");
          setFilteredCampaignListByStatus([]);
        }
      }
    } else {
      setFilteredCampaignListByStatus(
        filteredCampaignsList ? filteredCampaignsList : videosList
      );
    }
  };

  const campaignListByScreenId = useSelector(
    (state: any) => state.campaignListByScreenId
  );
  setTimeout(() => {
    setMyFilterError("");
  }, 4000);
  const {
    loading: loadingCampaign,
    error: errorCampaign,
    campaigns: videosList,
  } = campaignListByScreenId;
  useEffect(() => {
    if (loadingFilteredCampaign === false && filteredCampaigns?.length > 0) {
      setFilteredCampaignList(filteredCampaigns);
    } else if (
      loadingFilteredCampaign === false &&
      filteredCampaigns?.length === 0
    ) {
      setMyFilterError("No data Found, Try with different date range");
      setFilteredCampaignList([]);
      setStartDateHere(new Date());
      setEndDateHere(new Date());
    } else {
      dispatch({ type: FILTERED_CAMPAIGN_LIST_RESET });
    }
  }, [filteredCampaigns]);

  useEffect(() => {
    dispatch(getAllCampaignListByScreenId(screen._id));
  }, [dispatch]);

  return (
    <Box>
      <CampaignLogPopup
        show={showCampaignLogs}
        onHide={() => setShowCampaignLogs(false)}
        campaignLogs={campaignLogs}
      />
      {loadingCampaign || loadingFilteredCampaign ? (
        <HLoading loading={loadingCampaign || loadingFilteredCampaign} />
      ) : errorCampaign || errorFilteredCampaign ? (
        <MessageBox variant="danger">
          {errorCampaign || errorFilteredCampaign}
        </MessageBox>
      ) : (
        <Box>
          <Hide below="md">
            <Text
              color="#403F49"
              fontSize="lg"
              fontWeight="semibold"
              align="left"
            >
              {screen.name}
            </Text>
            <Text
              pt="10"
              color="#403F49"
              fontSize="lg"
              fontWeight="bold"
              align="left"
            >
              Filter
            </Text>
          </Hide>
          <Flex
            boxShadow="2xl"
            mt="5"
            px="2"
            gap={3}
            alignItems="center"
            justifyContent="flex-start"
            bgColor="#FFFFFF"
            borderRadius="8px"
          >
            <Stack py="5">
              <FormControl alignItems="center" id="startDateHere">
                <MuiPickersUtilsProvider utils={DateFnsUtils}>
                  <DateTimePicker
                    inputVariant="outlined"
                    disablePast={false}
                    format="dd/MM/yyyy hh:mm"
                    variant="dialog"
                    label={"From date"}
                    value={startDateHere}
                    onChange={setStartDateHere}
                    InputProps={{
                      endAdornment: (
                        <InputAdornment position="start">
                          <MiuiIconButton>
                            <AiOutlineDown />
                          </MiuiIconButton>
                        </InputAdornment>
                      ),
                    }}
                  />
                </MuiPickersUtilsProvider>
              </FormControl>
            </Stack>
            <Stack pl={{ base: "1", lg: "10" }}>
              <FormControl width="100%" alignItems="center" id="endDateHere">
                <MuiPickersUtilsProvider utils={DateFnsUtils}>
                  <DateTimePicker
                    inputVariant="outlined"
                    disablePast={false}
                    format="dd/MM/yyyy hh:mm"
                    variant="dialog"
                    label={"To date"}
                    value={endDateHere}
                    onChange={setEndDateHere}
                    InputProps={{
                      endAdornment: (
                        <InputAdornment position="start">
                          <MiuiIconButton>
                            <AiOutlineDown />
                          </MiuiIconButton>
                        </InputAdornment>
                      ),
                    }}
                  />
                </MuiPickersUtilsProvider>
              </FormControl>
            </Stack>

            <HStack pl={{ base: "1", lg: "10" }} align="center">
              <Button
                variant="outline"
                color="#403F49"
                py="3"
                onClick={filterCampaign}
                _hover={{ bg: "#82CD47", color: "#FFFFFF" }}
              >
                Search
              </Button>
              <Menu>
                <MenuButton
                  as={Button}
                  py="3"
                  bg="#FFFFFF"
                  color="#403F49"
                  _focus={{ boxShadow: "outline" }}
                  rightIcon={<AiOutlineDown />}
                >
                  Filter
                </MenuButton>
                <MenuList color="#403F49">
                  <MenuItem onClick={() => filterCampaigndateBySatus("All")}>
                    All
                  </MenuItem>
                  <MenuItem onClick={() => filterCampaigndateBySatus("Pause")}>
                    Pause
                  </MenuItem>
                  <MenuItem
                    onClick={() => filterCampaigndateBySatus("Pending")}
                  >
                    Pending
                  </MenuItem>
                  <MenuItem onClick={() => filterCampaigndateBySatus("Active")}>
                    Runing
                  </MenuItem>
                  <MenuItem
                    onClick={() => filterCampaigndateBySatus("Completed")}
                  >
                    Completed
                  </MenuItem>
                  <MenuItem
                    onClick={() => filterCampaigndateBySatus("Deleted")}
                  >
                    Deleted
                  </MenuItem>
                </MenuList>
              </Menu>
            </HStack>
          </Flex>
          <Stack pt="5" boxShadow="xl" borderRadius="8px">
            <Hide below="md">
              {myFilterError ? (
                <Text color="red" fontSize="md" fontWeight="semibold">
                  {myFilterError}
                </Text>
              ) : (
                <AdsListOfSinglScreen
                  videos={
                    filteredCampaignListByStatus?.length > 0
                      ? filteredCampaignListByStatus
                      : filteredCampaignsList?.length > 0
                      ? filteredCampaignsList
                      : videosList
                  }
                  handleShowCampaignLogs={handleShowCampaignLogs}
                />
              )}
            </Hide>
            <Show below="md">
              {myFilterError ? (
                <Text color="red" fontSize="md" fontWeight="semibold">
                  {myFilterError}
                </Text>
              ) : (
                <AdsListOfSignleScreenMobileView
                  videos={
                    filteredCampaignListByStatus?.length > 0
                      ? filteredCampaignListByStatus
                      : filteredCampaignsList?.length > 0
                      ? filteredCampaignsList
                      : videosList
                  }
                  handleShowCampaignLogs={handleShowCampaignLogs}
                />
              )}
            </Show>
          </Stack>
        </Box>
      )}
    </Box>
  );
}
