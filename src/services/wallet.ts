// import { Web } from "@_koi/sdk/web";
import { JWKInterface } from "arweave/web/lib/wallet";

import {
  encryptContentAndSave,
  retrieveAndDecryptContent,
} from "./secureContent";
import { ERROR_IDS } from "../utils/constants";
import Axios from "axios";
// import * as SolWeb3 from "@solana/web3.js";
// import bip39 from "bip39";
interface walletWithMnemonics {
  jwk: JWKInterface;
  mnemonics: string;
  walletAddress: string;
}

// const Solana = new SolWeb3.Connection(SolWeb3.clusterApiUrl("devnet"));

// const generateSolanaAddress = async () => {
//   let mnemonics = bip39.generateMnemonic();
//   console.log(mnemonics);
//   const seed = await bip39.mnemonicToSeed(mnemonic);
//   console.log(seed);
//   let a = new Uint8Array(seed.toJSON().data.slice(0, 32));
//   console.log(a);
//   var kp = SolWeb3.Keypair.fromSeed(a);
//   console.log(kp);
//   console.log(kp.publicKey.toBase58());
//   return {
//      jwk: a,
//      mnemonics: mnemonics
//      walletAddress: kp.publicKey.toBase58()
//   }
// };

export class WalletHelper {
  static generateWallet(user: any): Promise<any> {
    return Axios.post(
      `http://localhost:3333/api/credit/generateMnemonic/${user._id}`,
      {
        headers: {
          Authorization: `Bearer ${user.token}`,
        },
      }
    )
      .then(({ data }) => {
        localStorage.setItem("userInfo", JSON.stringify(data.user));
        return data;
      })
      .catch((error) => {
        console.log("error : ", error);
        return error;
      });
  }

  // return wallet.generateWallet(true).then(() => {
  //   return {
  //     jwk: wallet.wallet!,
  //     mnemonics: wallet.mnemonic!,
  //   };
  // });
  // let mnemonics = bip39.generateMnemonic();
  // console.log(mnemonics);
  // // const seed = await bip39.mnemonicToSeed(mnemonics);
  // bip39.mnemonicToSeed(mnemonics).then((res) => {
  //   console.log(res);
  //   // let a = new Uint8Array(res.toJSON().data.slice(0, 32));
  //   // var kp = SolWeb3.Keypair.fromSeed(a);
  //   // console.log(kp.publicKey.toBase58());
  //   // return {
  //   //   jwk: a,
  //   //   mnemonics: mnemonics,
  //   // };
  // });

  static importWallet(
    mnemonicsOrJwk: string | JWKInterface,
    wallet: any
  ): Promise<JWKInterface> {
    return wallet
      .loadWallet(mnemonicsOrJwk)
      .then(() => wallet.walletAddress)
      .catch((e: any) => {
        throw new Error(`${ERROR_IDS.WALLET_IMPORT}:${e}`);
      });
  }

  static generateAndSave(pin: string, user: any): Promise<any> {
    console.log("generateAndSave : ", pin);
    return this.generateWallet(user)
      .then(({ jwk, mnemonics }) =>
        encryptContentAndSave({ jwk, mnemonics }, pin).then(() => ({
          jwk,
          mnemonics,
        }))
      )
      .catch((error) => error);
  }

  static importAndSave(
    pin: string,
    mnemonics: string,
    wallet: any
  ): Promise<JWKInterface> {
    return this.importWallet(mnemonics, wallet).then((jwk) =>
      encryptContentAndSave({ jwk, mnemonics }, pin).then(() => jwk)
    );
  }

  static changePin(oldPin: string, newPin: string): Promise<void> {
    return retrieveAndDecryptContent(oldPin).then((data) =>
      encryptContentAndSave(data, newPin)
    );
  }
}
