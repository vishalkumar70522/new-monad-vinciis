import { Show, Hide } from "@chakra-ui/react";
import { CouponDetailsDesktopView } from "./CouponDetailsDesktopView";
import { CouponDetailsMobileView } from "./CouponDetailsMobileView";
import { useLocation } from "react-router-dom";

export function CouponDetails(props: any) {
  const location = useLocation();
  const couponData = location?.state?.couponData;
  return (
    <>
      <Hide below="md">
        <CouponDetailsDesktopView couponData={couponData} />
      </Hide>
      <Show below="md">
        <CouponDetailsMobileView couponData={couponData} />
      </Show>
    </>
  );
}
