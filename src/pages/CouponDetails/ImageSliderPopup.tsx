import { Box } from "@chakra-ui/react";
import { Modal } from "antd";
import SimpleImageSlider from "react-simple-image-slider";

export function ImageSliderPopup(props: any) {
  return (
    <Modal
      style={{ top: 50 }}
      footer={[]}
      open={props?.imageShow}
      onOk={() => props?.setImageShow(false)}
      onCancel={() => props?.setImageShow(false)}
    >
      <Box>
        <SimpleImageSlider
          width="90%"
          height="400px"
          images={props?.images}
          showBullets={true}
          showNavs={true}
          autoPlay={true}
        />
      </Box>
    </Modal>
  );
}
