import React, { useEffect, useState } from "react";
import {
  Stack,
  Flex,
  Table,
  TableContainer,
  Tbody,
  Td,
  Text,
  Th,
  Thead,
  Tr,
  Box,
  Hide,
  Show,
  Tooltip,
  IconButton,
  Badge,
} from "@chakra-ui/react";
import { convertIntoDateAndTime } from "../../utils/dateAndTime";
import { Form } from "react-bootstrap";
import { useDispatch, useSelector } from "react-redux";
import { changeCampaignStatus } from "../../Actions/campaignAction";
import HLoading from "../../components/atoms/HLoading";
import MessageBox from "../../components/atoms/MessageBox";
import { CAMPAIGN_DELETE_RESET } from "../../Constants/campaignConstants";
import { AdsListOfSignleScreenMobileView } from "../../components/common/AdsListOfSignleScreenMobileView";
import { BsPauseCircle } from "react-icons/bs";
import { RiDeleteBin6Line } from "react-icons/ri";
import { FaPlayCircle } from "react-icons/fa";

export function Actions(props: any) {
  const dispatch = useDispatch<any>();
  const { screen, videosList } = props;
  const [checkedItems, setCheckedItems] = useState(
    new Array(videosList.length).fill(false)
  );
  const allChecked = checkedItems.every(Boolean);

  const handleall = () => {
    const arr = new Array(videosList.length).fill(true);
    setCheckedItems([...arr]);
  };
  const handleSingle = (index: any) => {
    const newArray = [...checkedItems];
    newArray[index] = !newArray[index];
    setCheckedItems([...newArray]);
  };
  const campaignDelete = useSelector((state: any) => state.campaignDelete);
  const {
    loading: loadingCampaignDelete,
    error: errorCampaignDelete,
    success: changeCampaignStatusStatus,
  } = campaignDelete;

  if (changeCampaignStatusStatus) {
    //window.location.reload();
    props.handleSelectScreen(screen, props.selectedScreenIndex);
    dispatch({
      type: CAMPAIGN_DELETE_RESET,
    });
  }
  if (errorCampaignDelete) {
    setTimeout(() => {
      dispatch({
        type: CAMPAIGN_DELETE_RESET,
      });
    }, 2000);
  }

  const handleChangeCampaignStatus = (campaignId: any, status: any) => {
    dispatch(changeCampaignStatus(campaignId, status));
  };
  useEffect(() => {}, [props]);

  return (
    <Box>
      {loadingCampaignDelete ? (
        <HLoading loading={loadingCampaignDelete} />
      ) : null}
      {errorCampaignDelete ? (
        <MessageBox variant="danger">{errorCampaignDelete}</MessageBox>
      ) : null}
      <Box>
        <Hide below="md">
          <Text
            color="#403F49"
            fontSize="lg"
            fontWeight="semibold"
            align="left"
          >
            {screen.name}
          </Text>
          <Stack pt="10">
            <TableContainer borderRadius="5px" bgColor="#FFFFFF">
              <Table variant="simple">
                <Thead>
                  <Tr>
                    <Th>
                      <Flex>
                        <Form.Check
                          aria-label="option 1"
                          onChange={handleall}
                          checked={allChecked}
                        />
                        <Text pl="5">Brand</Text>
                      </Flex>
                    </Th>
                    <Th>Start date</Th>
                    <Th isNumeric>Total no of slots</Th>
                    <Th>Total charge</Th>
                    <Th>Status</Th>
                    <Th>Action</Th>
                  </Tr>
                </Thead>
                <Tbody>
                  {videosList.map((video: any, index: any) => (
                    <Tr key={index}>
                      <Td>
                        <Flex>
                          <Form.Check
                            aria-label="option 1"
                            onChange={() => handleSingle(index)}
                            checked={checkedItems[index]}
                          />
                          <Text color=" #403F49 " fontSize="sm" pl="5">
                            {video.campaignName}
                          </Text>
                        </Flex>
                      </Td>
                      <Td color="#575757" fontSize="sm">
                        {convertIntoDateAndTime(video.startDate)}
                      </Td>
                      <Td
                        isNumeric
                        fontSize="sm"
                        color="#403F49"
                        fontWeight="semibold"
                      >
                        {video.totalSlotBooked}
                      </Td>
                      <Td fontSize="sm" color="#403F49" fontWeight="semibold">
                        ₹{video.rentPerSlot * video.totalSlotBooked}
                      </Td>
                      <Td fontSize="sm" color="#403F49" fontWeight="semibold">
                        <Badge>{video.status}</Badge>
                      </Td>
                      <Td>
                        <Flex gap={3}>
                          {!video.isPause ? (
                            <Tooltip
                              hasArrow
                              label="Pause campaign"
                              fontSize="md"
                              bg="gray.300"
                              color="black"
                            >
                              <IconButton
                                bg="none"
                                icon={
                                  <BsPauseCircle size="25px" color="black" />
                                }
                                aria-label="Home"
                                onClick={() =>
                                  handleChangeCampaignStatus(video._id, "Pause")
                                }
                              ></IconButton>
                            </Tooltip>
                          ) : (
                            <Tooltip
                              hasArrow
                              label="Resume campaign"
                              fontSize="md"
                              bg="gray.300"
                              color="black"
                            >
                              <IconButton
                                bg="none"
                                icon={
                                  <FaPlayCircle size="25px" color="black" />
                                }
                                aria-label="Home"
                                onClick={() =>
                                  handleChangeCampaignStatus(
                                    video._id,
                                    "Resume"
                                  )
                                }
                              ></IconButton>
                            </Tooltip>
                          )}
                          <Tooltip
                            hasArrow
                            label="Delete campaign"
                            fontSize="md"
                            bg="gray.300"
                            color="black"
                          >
                            <IconButton
                              bg="nome"
                              icon={
                                <RiDeleteBin6Line size="25px" color="red" />
                              }
                              aria-label="Home"
                              onClick={() =>
                                handleChangeCampaignStatus(video._id, "Deleted")
                              }
                            ></IconButton>
                          </Tooltip>
                        </Flex>
                      </Td>
                    </Tr>
                  ))}
                </Tbody>
              </Table>
            </TableContainer>
          </Stack>
        </Hide>
        <Show below="md">
          <Stack pt="5">
            <AdsListOfSignleScreenMobileView
              videos={videosList}
              show={true}
              handleChangeCampaignStatus={(campaignId: any, status: any) =>
                handleChangeCampaignStatus(campaignId, status)
              }
            />
          </Stack>
        </Show>
      </Box>
    </Box>
  );
}
